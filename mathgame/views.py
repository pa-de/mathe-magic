from django.shortcuts import render
from .forms import AnswerForm
from .models import Point, Result
import random


# Create your views here.
def mathgame(request):
    angka_satu = str(random.randint(1,20))
    angka_dua = str(random.randint(1,20))
    operator = ['+', '-', '*']
    used_operator = operator[random.randint(0,2)]
    result = eval(angka_satu+used_operator+angka_dua)
    Result.objects.create(result=result)


    form = AnswerForm()
    if request.method == "POST":
        form = AnswerForm(request.POST)
        
        if (int(form['answer'].value())==int(Result.objects.order_by('-id')[1].result)):
            
            if (Point.objects.filter(name=form['name']).exists()):
            
                orang = Point.objects.get(name=form['name'])
                orang.points += 1
                orang.save()
            
                show = {
                    "angka_satu":angka_satu,
                    "operator":used_operator,
                    "angka_dua":angka_dua,
                    "answer_form": form,
                    'points':orang.points
                }
                return render(request, 'mathgame/mathgame.html', show)

    show = {
                "angka_satu":angka_satu,
                "operator":used_operator,
                "angka_dua":angka_dua,
                "answer_form": form,
            }
    return render(request, 'mathgame/mathgame.html', show)