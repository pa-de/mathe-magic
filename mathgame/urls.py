from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from . import views

app_name = 'mathgame'

urlpatterns = [
    path('', views.mathgame, name='mathgame'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

