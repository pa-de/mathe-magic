from django.test import TestCase, Client
from .models import Point,Result
from .forms import AnswerForm
from django.http import HttpResponse
from django.template.loader import render_to_string

# Create your tests here

class MathgameUnitTest(TestCase):
    def test_url_mathgame_exist(self):
        response = Client().get('/mathgame/')
        self.assertEqual(response.status_code, 200)

    def test_mathgame_using_mathgame_template(self):
        response = Client().get('/mathgame/')
        self.assertTemplateUsed(response, 'mathgame/mathgame.html')

    # def test_mathgame_answer(self):
    #     data = {
    #         'name':'praya',
    #         'answer': '20',
    #     }

    #     response = Client().post('/mathgame/',data)
    #     self.assertEqual(response.status_code, 302)