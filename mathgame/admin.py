from django.contrib import admin
from .models import Point, Result

# Register your models here.

admin.site.register(Point)
admin.site.register(Result)

