from django import forms

class AnswerForm(forms.Form):
    name = forms.CharField(label='Name', max_length=20, required=True)
    answer = forms.IntegerField(widget=forms.NumberInput(attrs={
        'class':'form-control', 'required':True, 'id':'answer'
    }), label='')