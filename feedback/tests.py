from django.test import TestCase, Client
from .models import PostModel
from .forms import PostForm

# Create your tests here.
class FeedbackUnitTest(TestCase):
    def test_url_feedback_exist(self):
        response = Client().get('/feedback/')
        self.assertEqual(response.status_code, 200)

    def test_feedback_using_feedback_templates(self):
        response = Client().get('/feedback/')
        self.assertTemplateUsed(response, 'feedback/feedback.html')

    def test_post_form(self):
        response = Client().get('/feedback/')
        self.assertContains(response, '<form')
        self.assertContains(response, '<button')

    def test_forms(self):
        data = {
            'name' : 'bambang',
            'message' : 'aiusfihasjk'
        }
        response = Client().post('/feedback/', data)
        self.assertEqual(response.status_code, 302)
    def test_url_show_exist(self):
        response = Client().get('/show/')
        self.assertEqual(response.status_code, 200)
