from django import forms
from .models import PostModel

class PostForm(forms.ModelForm):

    name = forms.CharField(widget=forms.TextInput(
        attrs= {
            'class': 'form-control',
            'placeholder': 'tell us, what is your name?',
            'size': '65%,',
        }
    ))

    message = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'let MatheMagic knows that ...',
        }
    ))

    class Meta:
        model = PostModel
        fields = [
            'name',
            'message',
        ]