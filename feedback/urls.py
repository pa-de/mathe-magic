#from django.conf.urls import url
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from . import views

app_name = 'feedback'

urlpatterns = [
    path("create/", views.create, name="feedback"),
    path("show/", views.show, name="show"),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)