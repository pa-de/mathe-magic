from django.shortcuts import render, redirect
from django.contrib import messages

# Create your views here.
from .forms import PostForm
from .models import PostModel

def show(request):
    posts = PostModel.objects.all()

    context = {
        'posts':posts,
    }

    return render(request, 'feedback/feedback-content.html', context)

def create(request):
    post_form = PostForm(request.POST or None)
    if request.method == 'POST': # POST request from browser
        if post_form.is_valid():
            post_form.save()
            messages.add_message(request, 20, "Yay! Your message successfully submitted!")
            return redirect('feedback')
        # else:
        #     messages.warning(request, "Oops, there's invalid! \n Please re-input peeps!")

    context = {
        'post_form':post_form,
    }

    return render(request, 'feedback/feedback.html', context)