from django.db import models
from django.utils import timezone


class discussionModels (models.Model):
    name = models.CharField(max_length = 10)
    discussion = models.CharField(max_length = 500)
    time = models.DateTimeField(default = timezone.now)

