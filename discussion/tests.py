from django.test import TestCase, Client
from django.urls import resolve
from .models import discussionModels
from .views import discussion

# Create your tests here.
class DiscussionUnitTest(TestCase):
    def test_discussion_url_is_exist(self):
        response = Client().get('/discussion/')
        self.assertEqual(response.status_code, 200)

    def test_discussion_using_discussion_func(self):
        found = resolve('/discussion/')
        self.assertEqual(found.func, discussion)

    def test_using_discussion_template(self):
        response = Client().get('/discussion/')
        self.assertTemplateUsed(response, 'discussion/discussion.html')

    def test_model_can_create_new_discussion(self):
        discussionModels.objects.create(name="John Doe", discussion="hello")
        counting = discussionModels.objects.all().count()
        self.assertEqual(counting, 1)

    def test_post_form(self):
        response = Client().get('/discussion/')
        self.assertContains(response, '<form')

    def test_forms(self):
        data = {
            'name' : 'john',
            'discussion' : 'hello world'
        }
        response = Client().post('/discussion/', data)
        self.assertEqual(response.status_code, 200)

    def test_default_forms(self):
        response = Client().post('/discussion/')
        self.assertEqual(response.status_code, 200)

