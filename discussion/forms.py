from django import forms
from .models import discussionModels

class discussionForms(forms.Form):
    name = forms.CharField(label='Your Name', max_length=10,
        widget=forms.TextInput(attrs={'class' : 'form-control marginbottom10 heightDiscussion', 'placeholder' : 'Max 20 character'}))

    discussion = forms.CharField(label='Discussion', max_length=500,
        widget=forms.TextInput(attrs={'class' : 'form-control marginbottom10', 'placeholder' : 'Tell us what you think'}))
    