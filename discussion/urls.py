from django.urls import path

from .views import discussion
# from django.conf.urls import url
from django.conf.urls.static import static

app_name = 'discussion'

urlpatterns = [
    path("discussion/", discussion, name="discussion"),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)