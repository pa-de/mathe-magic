from django.shortcuts import render
from .forms import discussionForms
from .models import discussionModels
from django.utils import timezone

def discussion(request):
    form = discussionForms()
    dictio = {}
    data = discussionModels.objects.all()
    if request.method == "POST":
        form = discussionForms(request.POST)
        dictio['form'] = form
        if form.is_valid():
            model = discussionModels(name = form['name'].value(), discussion = form['discussion'].value(),
            time = timezone.now())

            model.save()
            dictio['data'] = data
            return render(request, 'discussion/discussion.html', dictio)

    elif data.count != 0:
        return render(request, 'discussion/discussion.html', {'form': form, 'data': data})

    return render(request, 'discussion/discussion.html', {'form': form})

