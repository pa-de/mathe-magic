from django import forms
from django.db import models
from django.forms import ModelForm
from .models import NameModel

# isiNama = []
# if NameModel.objects.all().count() != 0:
#     for e in NameModel.objects.all().values():
#         isiNama.append((e['name'], e['name']))


class PickNameForm(ModelForm):
    namePick = forms.ModelChoiceField(queryset=NameModel.objects.all().order_by('name'))
    # namePick = forms.ModelChoiceField(choices=isiNama, widget=forms.Select(
    #     attrs= {'class' : 'form-control form-control-sm',
    #     'placeholder' : 'John Doe',
    #     }
    # ))
    class Meta:
        model = NameModel
        fields = ['namePick']

        
class RegisterNameForm(ModelForm):
    nameRegister = forms.CharField(widget=forms.TextInput(
        attrs= {
            'class' : 'form-control',
            'placeholder' : 'fill with your name',
        }
    ))

    class Meta:
        model = NameModel
        fields = ['nameRegister']