from django.shortcuts import render
from .forms import RegisterNameForm, PickNameForm
from .models import NameModel
from django.shortcuts import redirect

response = {}

# Create your views here.
def name_add(request):
    if request.method == 'POST' :
        form = RegisterNameForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('landing-page')
    else:
        form = PickNameForm()
    
    nama = request.session.get('nama')
    if not nama:
        nama = "Your Name"
    request.session['nama'] = nama
    
    response['nama'] = nama
    
    content = {'form' : form, 'response': response}
    
    return render(request, 'namelogin/register_name.html', content)

def name_pick(request):
    if request.method == 'POST' :
        form = PickNameForm(request.POST)
    else:
        form = PickNameForm()

    nama = request.session.get('nama')
    if not nama:
        nama = "Your Name"
    request.session['nama'] = nama
    
    response['nama'] = nama

    content = {'form' : form, 'response': response}

    return render(request, 'namelogin/pick_name.html', content)