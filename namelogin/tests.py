from django.test import TestCase, Client
from django.urls import resolve
from .models import NameModel
from .views import name_add, name_pick

# Create your tests here.
class NameloginUnitTest(TestCase):
    def test_welcome_url_is_exist(self):
        response = Client().get('/welcome/')
        self.assertEqual(response.status_code, 200)

    def test_register_url_is_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)
    
    def test_welcome_using_name_pick_func(self):
        found = resolve('/welcome/')
        self.assertEqual(found.func, name_pick)

    def test_namelogin_using_name_add_func(self):
        found = resolve('/register/')
        self.assertEqual(found.func, name_add)

    def test_using_pick_name_template(self):
        response = Client().get('/welcome/')
        self.assertTemplateUsed(response, 'namelogin/pick_name.html')

    def test_using_register_name_template(self):
        response = Client().get('/register/')
        self.assertTemplateUsed(response, 'namelogin/register_name.html')

    def test_model_can_create_new_namelogin(self):
        NameModel.objects.create(name="John Doe")
        counting = NameModel.objects.all().count()
        self.assertEqual(counting, 1)

    # def test_post_form(self):
    #     response = Client().get('/welcome/')
    #     self.assertContains(response, '<form')

    def test_post_form(self):
        response = Client().get('/register/')
        self.assertContains(response, '<form')
        
    def test_forms(self):
        data = {
            'name' : 'john',
        }
        response = Client().post('/welcome/', data)
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        data = {
            'nameRegister' : 'john',
        }
        response = Client().post('/register/', data)
        self.assertEqual(response.status_code, 302)
