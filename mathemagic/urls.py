"""mathemagic URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
import feedback.views
import discussion.views
import namelogin.views


admin.autodiscover()

app_name = 'feedback', 'mathgame', 'discussion'

urlpatterns = [
    path('admin/', admin.site.urls),
    # path('helloworld/', views.helloworld, name="helloworld")
    #path('', include('home.urls')),
    path('welcome/', namelogin.views.name_pick, name= 'landing-page'),
    path('', namelogin.views.name_pick, name= 'landing-page'),

   #path('mathgame/', include('mathgame.urls')),
    path('discussion/', discussion.views.discussion, name= 'discussion'),
    path('mathgame/', include('mathgame.urls')),
    
    path('discussion/', discussion.views.discussion, name= 'discussion'),
    path('feedback/', feedback.views.create, name="feedback"),
    path('show/', feedback.views.show, name="show"),
    path('register/', namelogin.views.name_add, name= 'register'),
]
