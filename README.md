# mathe-magic

[![Pipeline](https://gitlab.com/pa-de/mathe-magic/badges/master/pipeline.svg)](https://gitlab.com/pa-de/mathe-magic/)
[![Coverage](https://gitlab.com/pa-de/mathe-magic/badges/master/coverage.svg)](https://gitlab.com/pa-de/mathe-magic/)


## Team Member
Abhipraya Tjondronegoro | 1806191742 | Information System <br>
Eugene Brigita Lauw | 1806141183 | Computer Science <br>
Khadijah Rizqy Mufida | 1806235712 | Computer Science <br>
Muhamad Adhytia Wana Putra Rahmadhan | 1806141321 | Computer Science

## Heroku
This is our heroku app link, check it out [here](https://mathemagic-pade.herokuapp.com/)!

## Idea of Our Project
Mathemagic is a website for kids to learn about math in an easy-going and encouraging environment. Through mathemagic, users such as students, parents, and teachers can track how well they are doing, check how others are doing, share their problems, and have a discussion with other users regarding math.

## Website Features
**Name** your account to save your points <br>
**Leaderboard** enables you to see score from all users <br>
**Discussion Room** will be a place for users to share and give their opinions. Teachers can also interact with student through this feature <br>
**Feedback** feature will be implemented with the aim of receiving complaints from users, regarding the material and existing features.